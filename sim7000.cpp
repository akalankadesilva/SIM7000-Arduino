#include "sim7000.h"

void sim7000::init(Stream &serial,int pin){
  MODEM_SERIAL=&serial;
  MODEM_POWER_PIN=pin;
}

bool sim7000::powerON() {
  flushReadBuffer();
  MODEM_SERIAL->println(F("AT"));
  if(waitResponse("OK",1000)=="OK")return true;
  long start=millis();
  pinMode(MODEM_POWER_PIN, OUTPUT);
  while(millis()-start<MODEM_START_TIMEOUT){
      delay(200);
      digitalWrite(MODEM_POWER_PIN, HIGH);
      delay(2000);
      digitalWrite(MODEM_POWER_PIN, LOW);
      delay(4000);
      MODEM_SERIAL->println(F("AT"));
      if(waitResponse("OK",1000)=="OK")return true;
  }
  return false;
  
}


bool sim7000::powerOFF() {
  gprsReadyFlag=false;
  flushReadBuffer();
  MODEM_SERIAL->println(F("AT"));
  if(waitResponse("OK",1000)=="")return true;
  long start=millis();
  pinMode(MODEM_POWER_PIN, OUTPUT);
  while(millis()-start<MODEM_START_TIMEOUT){
      delay(200);
      digitalWrite(MODEM_POWER_PIN, HIGH);
      delay(2000);
      digitalWrite(MODEM_POWER_PIN, LOW);
      delay(4000);
      MODEM_SERIAL->println(F("AT"));
      if(waitResponse("OK",1000)=="")return true;
  }
  return false;
  
}



void sim7000::flushReadBuffer() {
  while (MODEM_SERIAL->available()) {
    MODEM_SERIAL->read();
  }
}

String sim7000::waitResponse(String response,int timeout,int length){
  long start=millis();
  String modemResponse;
  while(millis()-start<timeout){
    modemResponse=readLine();
      if((length<0&&modemResponse==response)||(length>0&&modemResponse.substring(0,length)==response)){
      return modemResponse;
    } 
  }
  return "";
}

String sim7000::readLine(bool special) {

  String line = "";
  bool b = true;
  
  while (b) {

    line = MODEM_SERIAL->readStringUntil('\r');
    if (line.charAt(0) == '\n')line = line.substring(1);
    Serial.print(">");
    Serial.println(line);

    if (line == F("SMS Ready")) {
      smsReadyFlag = true;
      
    }
    else if (line == "Call Ready") {
      callReadyFlag = true;
      
    }
    else if(line=="RDY"||line=="+CFUN: 1"||line=="+CPIN: READY"){

    }

    else if(line.substring(0,4)=="+FTP"){
      if(line.substring(4,7)=="GET"){
        if(line.charAt(9)=='1'&&line.charAt(11)=='1'){
          ftpOpenFlag = true;
          
         
        }
        else if(line.charAt(9)=='1'&&line.charAt(11)=='0'){
          ftpOpenFlag = false;
          
        }
        else if(line.charAt(9)=='2'){
          b=false;
        }
        else if(line.charAt(9)=='1'&&(line.charAt(11)=='6'||line.charAt(11)=='7')){
          ftpOpenErrorFlag = true;
          ftpOpenFlag = false;
          
        }
      }
      else if(line.substring(4,7)=="PUT"){
        if(line.charAt(9)=='1'&&line.charAt(11)=='1'){
          ftpOpenFlag = true;
          ftpWriteMax=(line.substring(13)).toInt();
          
        }
        else if(line.charAt(9)=='1'&&line.charAt(11)=='0'){
          ftpOpenFlag = false;
         
        }
        else if(line.charAt(9)=='2'){
          b=false;
        }
        else if(line.charAt(9)=='1'&&(line.charAt(11)=='6'||line.charAt(11)=='7')){
          ftpOpenErrorFlag = true;
          ftpOpenFlag = false;
         
        }
      }
      else if(line.substring(4,8)=="DELE"){
        if(line.charAt(10)=='1'&&line.charAt(12)=='1'){

        }
        else if(line.charAt(10)=='1'&&line.charAt(12)=='0'){
          ftpOpenFlag = false;
         
        }
        else if(line.charAt(10)=='1'&&(line.charAt(12)=='6'||line.charAt(12)=='7')){
          ftpOpenErrorFlag = true;
          ftpOpenFlag = false;
          
        }
      }
    }
 
    else if (line == "+SAPBR 1: DEACT") {
      gprsReadyFlag = false;
    }
    else if(line.substring(0,10)=="+SAPBR: 1,"){
        if(line.charAt(10)=='1'){
            gprsReadyFlag=true;

        }
        else{
          gprsReadyFlag=false;
        }
       
        
    }
    else if (line == "NORMAL POWER DOWN") {

    }
    else if(line.substring(0,12)=="+HTTPACTION:"){

      if(line.charAt(15)=='2')
        httpReadyFlag=true;
      else
        httpErrorFlag=true;

     
    }
    else {
      b = false;

    }
    if(!b&&special){
      b=false;
    }

  }

  return line;
}

bool sim7000::enableGPRS(String apn){
  this->apn=apn;
  MODEM_SERIAL->println(F("AT+SAPBR=2,1"));
  waitResponse("OK",1000);
  if(gprsReadyFlag)return true;

  MODEM_SERIAL->println(F("AT+SAPBR=3,1,\"Contype\",\"GPRS\""));
  waitResponse("OK",1000);
  MODEM_SERIAL->println("AT+SAPBR=3,1,\"APN\",\""+apn+"\"");
  waitResponse("OK",1000);
  long start;
  for(int i=0;i<ENABLE_GPRS_RETRY;i++){
    start=millis();
    while(millis()-start<ENABLE_GPRS_TIMEOUT){
      MODEM_SERIAL->println(F("AT+SAPBR=1,1"));
      if(waitResponse("ERROR",5000)==""){
        MODEM_SERIAL->println(F("AT+SAPBR=2,1"));
        waitResponse("OK",1000);
        if(gprsReadyFlag)return true;
      }
      delay(1000);
    }
    powerOFF();
    powerON();
  }
  return false;

}

bool sim7000::runCommands(String commandSeq[],String response[],int timeout,int commandCount){
  long start=millis();
  int wdt=0;
  for(int i=0;i<commandCount&&(millis()-start<timeout);i++){
    MODEM_SERIAL->print(commandSeq[i]);
   if(waitResponse(response[i],2000)!=response[i]){
    delay(1000);
    i--;
    wdt++;
    if(wdt>5){
      powerOFF();
      powerON();
      enableGPRS(apn);
      wdt=0;
    }
    continue;
   }
   else{
    wdt=0;
   }
  }
  if(millis()-start>timeout)return false;
  return true;
}

bool sim7000::ftpGETOpen(String apn,String ip, String username, String password, String path, String filename) {
  if(!enableGPRS(apn)) return false;
  String commandSeq[] = {F("AT+FTPCID=1\r\n"),"AT+FTPSERV=\"" + ip + "\"\r\n", "AT+FTPUN=\"" + username + "\"\r\n", "AT+FTPPW=\"" + password + "\"\r\n", "AT+FTPGETNAME=\"" + filename + "\"\r\n", "AT+FTPGETPATH=\""+path+"\"\r\n",F("AT+FTPGET=1\r\n")};
  String response[] = {F("OK"), F("OK"), F("OK"), F("OK"), F("OK"), F("OK"),F("OK")};
  
  if(!runCommands(commandSeq,response,FTP_GET_TIMEOUT,7))return false;
  
  ftpOpenFlag=false;
  long start=millis();
  while (!ftpOpenFlag) {
    readLine(true);
    if(ftpOpenErrorFlag||(millis()-start)>FTP_GET_TIMEOUT){
      ftpOpenErrorFlag=false;
      ftpOpenFlag=false;
      return false;
    }
    delay(100);
  }
  return true;


}

int sim7000::ftpGETRead(char* rxBuffer, int length) {
  MODEM_SERIAL->setTimeout(15000);

  if (!ftpOpenFlag)
    return -1;
  int readLength = -1;
  String line = "";
  while (readLength <= 0 && ftpOpenFlag) {
    MODEM_SERIAL->write("AT+FTPGET=2,");
    MODEM_SERIAL->println(length);
    if((line=waitResponse("+FTPGET: 2,",FTP_GET_TIMEOUT,11))!=""&&ftpOpenFlag){
      if (line.indexOf(",") < 0)return -2;
      readLength = (line.substring(line.indexOf(",") + 1)).toInt();

       MODEM_SERIAL->read();
      for (int i = 0; i < readLength; i++) {
        if (MODEM_SERIAL->available())
          rxBuffer[i] = MODEM_SERIAL->read();
        else {
          i = i - 1;
        }
      }
    }
    
    
  }
  delay(100);
  flushReadBuffer();
  return readLength;
}

bool sim7000::ftpPUTOpen(String apn,String ip, String username, String password, String path, String filename) {
   if(!enableGPRS(apn)) return false;
  String commandSeq[] = {F("AT+FTPCID=1\r\n"),"AT+FTPSERV=\"" + ip + "\"\r\n", "AT+FTPUN=\"" + username + "\"\r\n", "AT+FTPPW=\"" + password + "\"\r\n", "AT+FTPPUTNAME=\"" + filename + "\"\r\n", "AT+FTPPUTPATH=\""+path+"\"\r\n","AT+FTPPUT=1\r\n"};
  String response[] = {F("OK"), F("OK"), F("OK"), F("OK"), F("OK"), F("OK"),F("OK")};
  if(!runCommands(commandSeq,response,FTP_PUT_TIMEOUT,7))return false;

 ftpOpenFlag=false;
 long start=millis();
  while (!ftpOpenFlag) {
    readLine(true);
    if(ftpOpenErrorFlag||(millis()-start)>FTP_PUT_TIMEOUT){
      ftpOpenErrorFlag=false;
      ftpOpenFlag=false;
      return false;
    }
    delay(100);
  }
  return true;
}

bool sim7000::ftpPUTWrite(char* txBuffer, int length) {
  MODEM_SERIAL->setTimeout(15000);
  String line;
  long startTime=0;
  if (!ftpOpenFlag)
    return false;

  MODEM_SERIAL->write("AT+FTPPUT=2,");
  MODEM_SERIAL->println(length);
  waitResponse("+FTPPUT: 2",FTP_PUT_TIMEOUT,10);
  
  for(int i=0;i<length;i++){
    MODEM_SERIAL->write(txBuffer[i]);
  }
  delay(100);
  flushReadBuffer();
  return true;
}

void sim7000::ftpPUTClose(){
  long startTime=millis();
  MODEM_SERIAL->println("AT+FTPPUT=2,0");
  while(1){
    if(millis()-startTime>10000){
      startTime=millis();
      MODEM_SERIAL->println("AT+FTPPUT=2,0");
    }
    readLine(true);
    if(!ftpOpenFlag||ftpOpenErrorFlag){
      ftpOpenErrorFlag=false;
      break;
    }
  }
}



bool sim7000::ftpIsOpen(){
  return ftpOpenFlag;
}
bool sim7000::ftpIsError(){
  if(ftpOpenErrorFlag){
    ftpOpenErrorFlag=false;
    return true;
  }
  return false;
}
int sim7000::getFTPWriteMax(){
  return ftpWriteMax;
}

bool sim7000::ftpDelete(String apn,String ip,String username,String password,String path,String filename){
 MODEM_SERIAL->setTimeout(15000);
 if(!enableGPRS(apn)) return false;
 String commandSeq[] = {F("AT+FTPCID=1\r\n"),"AT+FTPSERV=\"" + ip + "\"\r\n", "AT+FTPUN=\"" + username + "\"\r\n", "AT+FTPPW=\"" + password + "\"\r\n", "AT+FTPGETNAME=\"" + filename + "\"\r\n",
 "AT+FTPGETPATH=\"" + path + "\"\r\n", F("AT+FTPDELE\r\n")
};
String response[] = {F("OK"), F("OK"), F("OK"), F("OK"), F("OK"), F("OK"), F("+FTPDELE: 1,0")};
 if(!runCommands(commandSeq,response,FTP_PUT_TIMEOUT,7))return false;
ftpReadyFlag=true;


long startTime=millis();
ftpOpenFlag=true;
while (ftpOpenFlag) {
  readLine(true);
  delay(1000);
  if((millis()-startTime)>FTP_DELETE_TIMEOUT||ftpOpenErrorFlag){
    ftpOpenFlag=false;
    ftpOpenErrorFlag=false;
    return false;
  }
}
ftpOpenFlag=false;
return true;


}


bool sim7000::httpPOST(String apn,String url,char* txBuffer,int length,String contentType){
 Serial.println(contentType);
 MODEM_SERIAL->setTimeout(15000);
 if(!enableGPRS(apn)) return false;
 String commandSeq[] = {F("AT+HTTPINIT\r\n"),F("AT+HTTPPARA=\"CID\",1\r\n"),"AT+HTTPPARA=\"CONTENT\",\""+contentType+"\"\r\n", "AT+HTTPPARA=\"URL\",\""+url+"\"\r\n", "AT+HTTPDATA="+String(length)+",10000\r\n"};
 String response[] = {F("OK"), F("OK"), F("OK"), F("OK") ,F("DOWNLOAD")};
  if(!runCommands(commandSeq,response,HTTP_POST_TIMEOUT,5))return false;

for(int i=0;i<length;i++){
  MODEM_SERIAL->write(txBuffer[i]);
}
while(readLine()!="OK"){
  delay(100);
}

MODEM_SERIAL->write("AT+HTTPACTION=1\r\n");
long startTime=millis();
httpReadyFlag=false;
while(!httpReadyFlag){
  readLine(true);
  delay(100);
  if(httpErrorFlag||(millis()-startTime)>HTTP_POST_TIMEOUT){
    httpErrorFlag=false;
    MODEM_SERIAL->write("AT+HTTPTERM\r\n");
    readLine();
    readLine();
    readLine();
    return false;
  }
}
httpReadyFlag=false;
MODEM_SERIAL->write("AT+HTTPTERM\r\n");
readLine();
readLine();
readLine();
return true;


}


bool sim7000::httpPOST(String apn,String url,String filename,String contentType){
 File dataFile = SD.open(filename, FILE_READ);
 char dataChar;
 MODEM_SERIAL->setTimeout(15000);
 if(!enableGPRS(apn)) return false;
 String commandSeq[] = {F("AT+HTTPINIT\r\n"),F("AT+HTTPPARA=\"CID\",1\r\n"),"AT+HTTPPARA=\"CONTENT\",\""+contentType+"\"\r\n", "AT+HTTPPARA=\"URL\",\""+url+"\"\r\n", "AT+HTTPDATA="+String(dataFile.size())+",10000\r\n"};
 String response[] = {F("OK"), F("OK"), F("OK"), F("OK") ,F("DOWNLOAD")};
 if(!runCommands(commandSeq,response,HTTP_POST_TIMEOUT,5))return false;

while((dataChar=dataFile.read())!=-1){
  MODEM_SERIAL->write(dataChar);
}
while(readLine()!="OK"){
  delay(100);
}

MODEM_SERIAL->write("AT+HTTPACTION=1\r\n");
long startTime=millis();
httpReadyFlag=false;
while(!httpReadyFlag){
  readLine(true);
  delay(100);
  if(httpErrorFlag||(millis()-startTime)>HTTP_POST_TIMEOUT){
    httpErrorFlag=false;
    MODEM_SERIAL->write("AT+HTTPTERM\r\n");
    readLine();
    readLine();
    readLine();
    return false;
  }
}
httpReadyFlag=false;
MODEM_SERIAL->write("AT+HTTPTERM\r\n");
readLine();
readLine();
readLine();
return true;


}


int sim7000::httpGET(String apn,String url,char* rxBuffer){
 MODEM_SERIAL->setTimeout(15000);
 if(!enableGPRS(apn)) return -1;
 String commandSeq[] = {F("AT+HTTPINIT\r\n"),F("AT+HTTPPARA=\"CID\",1\r\n"), "AT+HTTPPARA=\"URL\",\""+url+"\"\r\n"};
 String response[] = {F("OK"), F("OK"),  F("OK") };
 if(!runCommands(commandSeq,response,HTTP_GET_TIMEOUT,3))return false;

httpReadyFlag=false;
MODEM_SERIAL->write("AT+HTTPACTION=0\r\n");
long startTime=millis();
while(!httpReadyFlag){
  readLine(true);
  delay(100);
  if(httpErrorFlag||(millis()-startTime)>HTTP_GET_TIMEOUT){
    httpErrorFlag=false;
    gprsReadyFlag=false;
    MODEM_SERIAL->write("AT+HTTPTERM\r\n");
  readLine();
  readLine();
  readLine();
    return -1;
  }
}
httpReadyFlag=false;
MODEM_SERIAL->write("AT+HTTPREAD\r\n");
readLine();
readLine();
String line=readLine();
int readLength=(line.substring(11)).toInt();
Serial.print("Read Length ");
Serial.println(readLength);
for(int i=0;i<readLength;i++){
  if(MODEM_SERIAL->available())
    rxBuffer[i]=MODEM_SERIAL->read();

  else
    --i;
}
delay(1000);
flushReadBuffer();
MODEM_SERIAL->write("AT+HTTPTERM\r\n");
readLine();
readLine();
readLine();
return readLength;


}

int sim7000::getCSQ(){
  MODEM_SERIAL->println("AT+CSQ");
  delay(100);
  readLine();
  readLine();
  String line=readLine();
  readLine();
  if(line.charAt(7)==-1)return 0;
  return (((int)line.charAt(6))-48)*10+(int)line.charAt(7)-48;
}

bool sim7000::getGPRSStatus(){
	return gprsReadyFlag;
}
